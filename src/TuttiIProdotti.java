import java.awt.BorderLayout;
import java.sql.*;
import java.util.logging.*;
import javax.swing.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author T135837
 */
public class TuttiIProdotti extends javax.swing.JFrame {

    /**
     * Creates new form TuttiIProdotti
     */
    Connessione conn;
    ResultSetTable table = null;
    JScrollPane tableContainer;
    ResultSet rs;
    private JTable t;
    public TuttiIProdotti() {
        
        initComponents();
    }
    public TuttiIProdotti(Connessione conn) {
                  
        initComponents();
        this.conn=conn;
        try {
            rs = conn.executeQuery("select * from prodotto");
            table = new ResultSetTable(rs);
            table.setAutoCreateRowSorter(true);
            table.setDragEnabled(false);
            panel.setLayout(new BorderLayout());
            tableContainer = new JScrollPane(table);
            panel.add(tableContainer, BorderLayout.CENTER);
            this.setLocationRelativeTo(null);
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panel = new javax.swing.JPanel();
        bEliminaProdotto = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Tutti i prodotti");

        javax.swing.GroupLayout panelLayout = new javax.swing.GroupLayout(panel);
        panel.setLayout(panelLayout);
        panelLayout.setHorizontalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        panelLayout.setVerticalGroup(
            panelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 271, Short.MAX_VALUE)
        );

        bEliminaProdotto.setText("Elimina Prodotto selezionato");
        bEliminaProdotto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bEliminaProdottoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 233, Short.MAX_VALUE)
                .addComponent(bEliminaProdotto))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(panel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bEliminaProdotto))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void reset(){
        try {
            panel.removeAll();
            rs = conn.executeQuery("select * from prodotto");
            table = new ResultSetTable(rs);
            table.setAutoCreateRowSorter(true);
            table.setDragEnabled(false);
            panel.setLayout(new BorderLayout());
            tableContainer = new JScrollPane(table);
            panel.add(tableContainer, BorderLayout.CENTER);
            panel.revalidate();
            panel.repaint();
        } catch (SQLException ex) {
            Logger.getLogger(TuttiIProdotti.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void bEliminaProdottoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bEliminaProdottoActionPerformed
        int riga = table.getSelectedRow();
        if(riga!=-1){
            String id = (String) table.getValueAt(riga, 0);
            try {
                conn.execute("DELETE FROM PRODOTTO WHERE CODICE='"+id+"';");
                JOptionPane.showMessageDialog(null, "Prodotto eliminato con successo","Successo",JOptionPane.PLAIN_MESSAGE);
            } catch (SQLException ex) {

                Logger.getLogger(TuttiIProdotti.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(null, "Prodotto non eliminato, contattare il responsabile","ERRORE",JOptionPane.ERROR_MESSAGE);
            }
            reset(); 
        }else{
            JOptionPane.showMessageDialog(null, "Nessun prodotto selezionato","Attenzione",JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_bEliminaProdottoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(TuttiIProdotti.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(TuttiIProdotti.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(TuttiIProdotti.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(TuttiIProdotti.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TuttiIProdotti().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bEliminaProdotto;
    private javax.swing.JPanel panel;
    // End of variables declaration//GEN-END:variables
}
