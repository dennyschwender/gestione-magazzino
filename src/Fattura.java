import com.itextpdf.text.DocumentException;
import java.awt.event.*;
import java.io.IOException;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.logging.*;
import javax.swing.*;
import javax.swing.table.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author T135837
 */
public class Fattura extends javax.swing.JFrame {

    /**
     * Creates new form Fattura
     */
    Connessione conn;
    ResultSet rs;
    public Fattura() {
        initComponents();
        this.setLocationRelativeTo(null);
    }
    
    public Fattura(final Connessione conn) {
        initComponents();
        this.setLocationRelativeTo(null);
        this.conn=conn;
        String[] months = new DateFormatSymbols(Locale.ITALIAN).getMonths();
        Calendar cal = Calendar.getInstance();
        tfData.setText((new SimpleDateFormat("d").format(cal.getTime()))+ " "+ months[Integer.parseInt(new SimpleDateFormat("M").format(cal.getTime()))-1]+ " "+(new SimpleDateFormat("yyyy").format(cal.getTime())));
        tfScontoTotale.setText("0");
        try {
            rs=conn.executeQuery("select nome,cognome from cliente order by cognome");
            while(rs.next()){
                cbCliente.addItem(rs.getString("cognome")+" "+rs.getString("nome"));
            }
            rs=conn.executeQuery("select nome from fornitore order by nome");
            while(rs.next()){
                cbNegozio.addItem(rs.getString("nome"));
            }
            rs=conn.executeQuery("select codice from prodotto order by codice");
            while(rs.next()){
                cbCodiceProdotto.addItem(rs.getString("codice"));
            }
            rs=conn.executeQuery("select codice,descrizione from prodotto order by codice");
            while(rs.next()){
                cbDescrizioneProdotto.addItem(rs.getString("descrizione"));
            }
            AutoCompletion.enable(cbCliente);
            AutoCompletion.enable(cbCodiceProdotto); 
            AutoCompletion.enable(cbDescrizioneProdotto); 
            AutoCompletion.enable(cbNegozio);
        } catch (SQLException ex) {
            Logger.getLogger(Fattura.class.getName()).log(Level.SEVERE, null, ex);
        }
        cbCodiceProdotto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(cbCodiceProdotto.getSelectedIndex()!=0){
                    cbDescrizioneProdotto.setSelectedIndex(0);
                }
            }
        });
        cbDescrizioneProdotto.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(cbDescrizioneProdotto.getSelectedIndex()!=0){
                    cbCodiceProdotto.setSelectedIndex(0);
                }
            }
        }); 
        cbCliente.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(cbCliente.getSelectedIndex()!=0){
                    cbNegozio.setSelectedIndex(0);
                    try {
                    String[] dati;  
                    dati = cbCliente.getSelectedItem().toString().split("\\s+");
                    rs = conn.executeQuery("SELECT SCONTO FROM CLIENTE WHERE NOME = '"+dati[1]+"' AND COGNOME = '"+dati[0]+"';");
                    rs.first();
                    tfScontoTotale.setText(rs.getString("sconto"));
                    aggiornaTotale();
                    } catch (SQLException ex) {
                        Logger.getLogger(Fattura.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            }
        });
        cbNegozio.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(cbNegozio.getSelectedIndex()!=0){
                    cbCliente.setSelectedIndex(0);
                    try {
                    rs = conn.executeQuery("SELECT SCONTO FROM FORNITORE WHERE NOME = '"+cbNegozio.getSelectedItem().toString()+"';");
                    rs.first();
                    tfScontoTotale.setText(rs.getString("sconto"));
                    aggiornaTotale();
                    } catch (SQLException ex) {
                        Logger.getLogger(Fattura.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                
            }
        }); 
        Action action;
        action = new AbstractAction(){
            @Override
            public void actionPerformed(ActionEvent e){
                TableCellListener tcl = (TableCellListener)e.getSource();
                DefaultTableModel model = (DefaultTableModel)table.getModel();
                if(tcl.getColumn()==4){
                    if((int)tcl.getNewValue()>=0&&(int)tcl.getNewValue()<=100){
                       double totale = Double.parseDouble(model.getValueAt(tcl.getRow(), tcl.getColumn()-2)+"");
                       model.setValueAt((totale-(totale/100*(int)tcl.getNewValue()))*(int)model.getValueAt(tcl.getRow(), tcl.getColumn()-1), tcl.getRow(), tcl.getColumn()+1);
                    }else{
                        model.setValueAt(tcl.getOldValue(), tcl.getRow(), tcl.getColumn());
                        JOptionPane.showMessageDialog(null, "Sconto non compreso tra 0 e 100","ATTENZIONE",JOptionPane.WARNING_MESSAGE);
                    }             
                }
                if(tcl.getColumn()==3){
                    if((int)tcl.getNewValue()>0){
                        model.setValueAt((Double.parseDouble(model.getValueAt(tcl.getRow(), tcl.getColumn()-1)+"")*(int)tcl.getNewValue())/100*(100-(int)model.getValueAt(tcl.getRow(), tcl.getColumn()+1)), tcl.getRow(), tcl.getColumn()+2);
                    }else{
                        model.setValueAt(tcl.getOldValue(), tcl.getRow(), tcl.getColumn());
                    }        
                }
                table.setModel(model);
                aggiornaTotale();
            }
        };
        tfScontoTotale.addKeyListener(new KeyAdapter() {  
         @Override
         public void keyTyped(KeyEvent e) {  
           char c = e.getKeyChar();  
           if (!(Character.isDigit(c) || (c == KeyEvent.VK_BACK_SPACE) ||  (c == KeyEvent.VK_DELETE))) {  
                e.consume();
                aggiornaTotale();
              }  
         }  
       });
       new TableCellListener(table, action);
    }
    private String monetizza(float valore){
        DecimalFormat dec = new DecimalFormat("#.00");
        String temp2 = dec.format(valore);
        int last = Integer.parseInt(temp2.substring(temp2.length()-1));
        int prelast = Integer.parseInt(temp2.substring(temp2.length()-2,temp2.length()-1));
        if(last==0||last==5){
            return String.valueOf(temp2);
        }else if(last<5){
            if(last>2){
                last=5;
            }else{
                last=0;
            }
        }else{
            if(last>7){
                last=0;
                prelast +=1;
            }else{
                last=5;
            }
        }
        return temp2.substring(0,temp2.length()-2)+prelast+last;
    }
    private void aggiornaTotale(){
        DefaultTableModel model = (DefaultTableModel)table.getModel();
        float totale = 0;
        for(int i=0;i<model.getRowCount();i++){
             totale +=  Double.parseDouble(model.getValueAt(i, 5)+"");
        }
        if(Integer.parseInt(tfScontoTotale.getText())>=0&&Integer.parseInt(tfScontoTotale.getText())<=100){
            tfTotale.setText(monetizza(totale/100*(100-Integer.parseInt(tfScontoTotale.getText()))));
        }else{
            JOptionPane.showMessageDialog(null, "Sconto totale non compreso tra 0 e 100","ATTENZIONE",JOptionPane.WARNING_MESSAGE);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        bindingGroup = new org.jdesktop.beansbinding.BindingGroup();

        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        lData = new javax.swing.JLabel();
        tfData = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        cbCliente = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        tfScontoTotale = new javax.swing.JTextField();
        lTotale = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        tfTotale = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();
        bInserisci = new javax.swing.JButton();
        lProdotto = new javax.swing.JLabel();
        cbCodiceProdotto = new javax.swing.JComboBox();
        bFattura = new javax.swing.JButton();
        cbDescrizioneProdotto = new javax.swing.JComboBox();
        lCodiceProdotto = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        taNote = new javax.swing.JTextArea();
        lNote = new javax.swing.JLabel();
        lNegozio = new javax.swing.JLabel();
        cbNegozio = new javax.swing.JComboBox();
        bTogliProdotto = new javax.swing.JButton();

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane2.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Gestione Magazzino - Fattura");

        lData.setText("Data");

        jLabel1.setText("Cliente");

        cbCliente.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--Nessuno--" }));

        jLabel2.setText("Sconto totale:");

        lTotale.setText("Totale:");

        tfTotale.setEditable(false);

        table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codice", "Prodotto", "Prezzo", "Quantità", "Sconto", "Totale"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        table.getTableHeader().setReorderingAllowed(false);

        org.jdesktop.beansbinding.Binding binding = org.jdesktop.beansbinding.Bindings.createAutoBinding(org.jdesktop.beansbinding.AutoBinding.UpdateStrategy.READ_WRITE, this, org.jdesktop.beansbinding.ObjectProperty.create(), table, org.jdesktop.beansbinding.BeanProperty.create("selectedElements"), "");
        bindingGroup.addBinding(binding);

        jScrollPane1.setViewportView(table);

        bInserisci.setText("Inserisci prodotto");
        bInserisci.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bInserisciActionPerformed(evt);
            }
        });

        lProdotto.setText("Prodotto");

        cbCodiceProdotto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--Nessuno--" }));

        bFattura.setText("Fattura");
        bFattura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bFatturaActionPerformed(evt);
            }
        });

        cbDescrizioneProdotto.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--Nessuno--" }));

        lCodiceProdotto.setText("Codice");

        jLabel3.setText("Descrizione");

        taNote.setColumns(20);
        taNote.setLineWrap(true);
        taNote.setRows(3);
        taNote.setWrapStyleWord(true);
        jScrollPane3.setViewportView(taNote);

        lNote.setText("Note");

        lNegozio.setText("Negozio");

        cbNegozio.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--Nessuno--" }));

        bTogliProdotto.setText("Togli prodotto");
        bTogliProdotto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bTogliProdottoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lData)
                                    .addComponent(tfData, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lProdotto))
                                .addGap(76, 76, 76))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1)
                                    .addComponent(cbCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lNegozio)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addComponent(cbNegozio, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfScontoTotale, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(168, 168, 168)
                        .addComponent(lTotale)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tfTotale, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(bTogliProdotto)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(121, 121, 121))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(bFattura)
                                .addContainerGap())))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(cbCodiceProdotto, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(cbDescrizioneProdotto, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(28, 28, 28)
                                        .addComponent(bInserisci))))
                            .addComponent(lCodiceProdotto))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jScrollPane3)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(lNote)
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap())))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lData)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tfData, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lNegozio)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cbNegozio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addComponent(lProdotto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lCodiceProdotto)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bInserisci)
                    .addComponent(cbCodiceProdotto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cbDescrizioneProdotto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bTogliProdotto)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addComponent(lNote)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(tfScontoTotale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lTotale)
                    .addComponent(tfTotale, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bFattura, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );

        bindingGroup.bind();

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void bInserisciActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bInserisciActionPerformed
        if(cbCodiceProdotto.getSelectedIndex()!=0){
            try {
                
                rs=conn.executeQuery("SELECT CODICE , DESCRIZIONE ,PREZZO  FROM PRODOTTO WHERE CODICE = '"+cbCodiceProdotto.getSelectedItem().toString()+"';");
                rs.first();
                DefaultTableModel model = (DefaultTableModel)table.getModel();
                Object[] rowData = new Object[table.getColumnCount()];
                rowData[0]=rs.getString("CODICE");
                rowData[1]=rs.getString("DESCRIZIONE");
                rowData[2]=monetizza(rs.getFloat("PREZZO"));
                rowData[3]=1;
                rowData[4]=0;
                rowData[5]=monetizza((int)rowData[3]*rs.getFloat("PREZZO")-(rs.getFloat("PREZZO")/100*(int)rowData[4]));
                model.addRow(rowData);
                table.setModel(model);
            } catch (SQLException ex) {
                Logger.getLogger(Fattura.class.getName()).log(Level.SEVERE, null, ex);
            }
            aggiornaTotale();
        }else if(cbDescrizioneProdotto.getSelectedIndex()!=0){
            try {
                rs=conn.executeQuery("SELECT CODICE , DESCRIZIONE ,PREZZO  FROM PRODOTTO WHERE DESCRIZIONE = '"+cbDescrizioneProdotto.getSelectedItem().toString()+"';");
                rs.first();
                DefaultTableModel model = (DefaultTableModel)table.getModel();
                Object[] rowData = new Object[table.getColumnCount()];
                rowData[0]=rs.getString("CODICE");
                rowData[1]=rs.getString("DESCRIZIONE");
                rowData[2]=rs.getInt("PREZZO");
                rowData[3]=1;
                rowData[4]=0;
                rowData[5]=(int)rowData[3]*rs.getInt("PREZZO")-(rs.getInt("PREZZO")/100*(int)rowData[3]);
                model.addRow(rowData);
                table.setModel(model);
            } catch (SQLException ex) {
                Logger.getLogger(Fattura.class.getName()).log(Level.SEVERE, null, ex);
            }
            aggiornaTotale();
        }
    }//GEN-LAST:event_bInserisciActionPerformed

    private void bFatturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bFatturaActionPerformed
        if(!tfData.getText().isEmpty()){
            if(cbCliente.getSelectedIndex()>0||cbNegozio.getSelectedIndex()>0){
                if(table.getModel().getRowCount()>0){
                    try {
                        String[] dati;
                        String[] cliente = new String[5];
                        String tipo="cliente";
                        dati = cbCliente.getSelectedItem().toString().split("\\s+");
                        if(cbCliente.getSelectedIndex()!=0){
                            rs = conn.executeQuery("SELECT NOME ,COGNOME ,VIA ,CAP ,PAESE FROM CLIENTE WHERE COGNOME = '"+dati[0]+"' and nome = '"+dati[1]+"';");
                            rs.first();
                            cliente[0] = rs.getString("NOME");
                            cliente[1] = rs.getString("COGNOME");
                            cliente[2] = rs.getString("VIA");
                            cliente[3] = rs.getString("CAP");
                            cliente[4] = rs.getString("PAESE");
                            tipo = "cliente";
                        }else if(cbNegozio.getSelectedIndex()!=0){
                            rs = conn.executeQuery("SELECT NOME, VIA ,CAP ,PAESE,(SELECT NOME||' '||COGNOME FROM CLIENTE WHERE CODICE = CODRESPONSABILE) AS \"RESPONSABILE\" FROM FORNITORE WHERE NOME = '"+cbNegozio.getSelectedItem().toString()+"';");
                            rs.first();
                            cliente[0] = rs.getString("NOME");
                            if(rs.getString("RESPONSABILE") != null){
                               cliente[1] = rs.getString("RESPONSABILE");
                            }else{
                                cliente[1] = "";
                            }
                            cliente[2] = rs.getString("VIA");
                            cliente[3] = rs.getString("CAP");
                            cliente[4] = rs.getString("PAESE");
                            tipo="negozio";
                        }
                        String data = tfData.getText();
                        String scontoTotale = tfScontoTotale.getText();
                        String[][] prodotto=new String[table.getModel().getRowCount()][table.getModel().getColumnCount()];
                        for(int i=0;i<table.getModel().getRowCount();i++){
                            for(int j=0;j<table.getModel().getColumnCount();j++){
                                prodotto[i][j]=table.getModel().getValueAt(i, j).toString();
                            }
                        }
                        new PdfFattura(cliente, data, scontoTotale, prodotto, taNote.getText(), tfTotale.getText(),tipo).createPdf();
                       conn.execute("MERGE INTO FATTURA VALUES((SELECT MAX(CODICE)+1 FROM FATTURA),(SELECT CODICE FROM CLIENTE WHERE NOME ='"+cliente[0]+"' AND COGNOME='"+cliente[1]+"'),'"+taNote.getText()+"','Franchi',"+scontoTotale+");");
                       for(int i=0;i<prodotto.length;i++){
                         conn.execute("MERGE INTO PRODOTTOFATTURA  VALUES((SELECT MAX(CODICE) FROM FATTURA),'"+prodotto[i][0]+"',"+prodotto[i][4]+","+prodotto[i][3]+");"); 
                         conn.execute("MERGE INTO PRODOTTO(CODICE,QTAMAG) VALUES('"+prodotto[i][0]+"',(SELECT QTAMAG-"+prodotto[i][3]+" FROM PRODOTTO WHERE CODICE = '"+prodotto[i][0]+"'));");
                       }
                    } catch (SQLException | DocumentException | IOException ex) {
                        Logger.getLogger(Fattura.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Nessun prodotto scelto","ATTENZIONE",JOptionPane.WARNING_MESSAGE); 
                }
            }else{
                JOptionPane.showMessageDialog(null, "Nessun cliente scelto","ATTENZIONE",JOptionPane.WARNING_MESSAGE); 
            }
        }else{
           JOptionPane.showMessageDialog(null, "Nessuna data immessa","ATTENZIONE",JOptionPane.WARNING_MESSAGE); 
        }
    }//GEN-LAST:event_bFatturaActionPerformed

    private void bTogliProdottoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bTogliProdottoActionPerformed
        int riga = table.getSelectedRow();
        if(riga!=-1){
            DefaultTableModel model = (DefaultTableModel)table.getModel();
            model.removeRow(riga);
            table.setModel(model);
            aggiornaTotale();
        }else{
            JOptionPane.showMessageDialog(null, "Nessun prodotto selezionato","Attenzione",JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_bTogliProdottoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Fattura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Fattura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Fattura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Fattura.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Fattura().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bFattura;
    private javax.swing.JButton bInserisci;
    private javax.swing.JButton bTogliProdotto;
    private javax.swing.JComboBox cbCliente;
    private javax.swing.JComboBox cbCodiceProdotto;
    private javax.swing.JComboBox cbDescrizioneProdotto;
    private javax.swing.JComboBox cbNegozio;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JLabel lCodiceProdotto;
    private javax.swing.JLabel lData;
    private javax.swing.JLabel lNegozio;
    private javax.swing.JLabel lNote;
    private javax.swing.JLabel lProdotto;
    private javax.swing.JLabel lTotale;
    private javax.swing.JTextArea taNote;
    private javax.swing.JTable table;
    private javax.swing.JTextField tfData;
    private javax.swing.JTextField tfScontoTotale;
    private javax.swing.JTextField tfTotale;
    private org.jdesktop.beansbinding.BindingGroup bindingGroup;
    // End of variables declaration//GEN-END:variables
}
