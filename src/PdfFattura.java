import java.io.FileOutputStream;
import java.io.IOException;
import com.itextpdf.text.*;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.*;
import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.logging.*;

/**
 * First iText example: Hello World.
 */
public class PdfFattura extends PdfPageEventHelper {
    static PdfWriter writer;
    PdfContentByte cb;
    static Document document;
    String[] cliente;
    String data;
    String scontoTotale;
    String[][] prodotto;
    String note;
    String totale;
    Calendar cal = Calendar.getInstance();
    static SimpleDateFormat s = new SimpleDateFormat("dd-MM-yyyy_kk-mm-ss", Locale.ITALIAN);
    String cartella = System.getProperty("user.home")+"/Documents/Fatture/";
    String result,tipo;
    public PdfFattura(String[] cliente,String data,String scontoTotale,String[][] prodotto, String note, String totale,String tipo){
        this.cliente=cliente;
        this.data=data;
        this.scontoTotale=scontoTotale;
        this.prodotto=prodotto;
        this.note=note;
        this.totale=totale;
        this.tipo = tipo;
    }
    public PdfFattura(){}
    /**
     * Creates a PDF file: hello.pdf
     * @param    args    no arguments needed
     */
    public static void main(String[] args)
    	throws DocumentException, IOException {
    	new PdfFattura().createPdf();
    }
    
    /**
     * Creates a PDF document.
     * @throws    DocumentException 
     * @throws    IOException 
     */
    private String monetizza(float valore){
        DecimalFormat dec = new DecimalFormat("#.00");
        String temp2 = dec.format(valore);
        int last = Integer.parseInt(temp2.substring(temp2.length()-1));
        int prelast = Integer.parseInt(temp2.substring(temp2.length()-2,temp2.length()-1));
        if(last==0||last==5){
            return String.valueOf(temp2);
        }else if(last<5){
            if(last>2){
                last=5;
            }else{
                last=0;
            }
        }else{
            if(last>7){
                last=0;
                prelast +=1;
            }else{
                last=5;
            }
        }
        return temp2.substring(0,temp2.length()-2)+prelast+last;
    }
    
    public void createPdf()
	throws DocumentException, IOException {
        // step 1
        result = System.getProperty("user.home")+"/Documents/Fatture/"+s.format(cal.getTime())+"_"+cliente[0]+" "+cliente[1]+".pdf";
        document = new Document();
        document.setMargins(20,0 ,40,0);
        // step 2
        File file = new File(cartella);
        file.mkdirs();
        writer = PdfWriter.getInstance(document, new FileOutputStream(result));
        // step 3
        document.open();
        // step 4
        cb = writer.getDirectContent();
        cb.beginText();
        cb.setFontAndSize(BaseFont.createFont(), (float) 10.5);
        
        //Stampa Intestazione
        cb.moveText(40, document.top()-20);
        cb.newlineShowText("Merceria Kuki");
        cb.moveText(0, -10);
        cb.newlineShowText("di Aline Schwender");
        cb.setFontAndSize(BaseFont.createFont(), (float) 10);
        cb.moveText(0, -30);
        cb.newlineShowText("Fornitura di articoli per creazioni esclusive");
        //Fine Intestazione
        //Stampa indirizzo cliente
        if(tipo.equals("cliente")){
            cb.moveText(315, -70);
            cb.newlineShowText(cliente[0]+" "+cliente[1]);
            cb.moveText(0, -10);
            cb.newlineShowText(cliente[2]);
            cb.moveText(0, -10);
            cb.newlineShowText(cliente[3]+" "+cliente[4]);
        }else if(tipo.equals("negozio")){
            if(!cliente[1].equals("")){
                cb.moveText(315, -70);
                cb.newlineShowText(cliente[0]);
                cb.moveText(0, -10);
                cb.newlineShowText(cliente[1]);
                cb.moveText(0, -10);
                cb.newlineShowText(cliente[2]);
                cb.moveText(0, -10);
                cb.newlineShowText(cliente[3]+" "+cliente[4]);
            }else{
                cb.moveText(315, -70);
                cb.newlineShowText(cliente[0]);
                /*cb.moveText(0, -10);
                cb.newlineShowText(cliente[1]);*/
                cb.moveText(0, -10);
                cb.newlineShowText(cliente[2]);
                cb.moveText(0, -10);
                cb.newlineShowText(cliente[3]+" "+cliente[4]);
            }
        }
        //Fine Indirizzo Cliente
        //Stampa Data
        cb.moveText(-315, -30);
        cb.newlineShowText("Quartino, il "+data);
        //Fine Data
        //Stampa tabella prodotti
        //Inizio intestazione tabella
        cb.moveText(0, -30);
        cb.newlineShowText("Codice");
        cb.moveText(80, 0);
        cb.newlineShowText("Prodotto");
        cb.moveText(200, 0);
        cb.newlineShowText("Prezzo unitario");
        cb.moveText(80, 0);
        cb.newlineShowText("Quantità");
        cb.moveText(80, 0);
        cb.newlineShowText("Sconto");
        cb.moveText(50, 0);
        cb.newlineShowText("Totale");  
        //Fine Intestazione
        //Inizio prodotti
        int[] misure=new int[6];
        misure[0]=-490;
        misure[1]=80;
        misure[2]=200;
        misure[3]=80;
        misure[4]=80;
        misure[5]=50;
        for(int i=0;i<prodotto.length;i++){
            for(int j=0;j<prodotto[0].length;j++){
                if(j==0){
                    cb.moveText(misure[j], -20);
                    cb.newlineShowText(prodotto[i][j]);
                }else if(j==1){
                    int controlValue = 35;
                    if(prodotto[i][j].length()>controlValue){
                        for(int k=0;k<prodotto[i][j].length();k+=controlValue){
                            if(k!=0){
                                cb.moveText(0, -20);
                                if(k+controlValue<=prodotto[i][j].length()){                                    
                                    cb.newlineShowText(prodotto[i][j].substring(k, k+controlValue));
                                }else{
                                    cb.newlineShowText(prodotto[i][j].substring(k));
                                }
                            }else{
                                cb.moveText(misure[j], 0);
                                if(k+controlValue<=prodotto[i][j].length()){                                    
                                    cb.newlineShowText(prodotto[i][j].substring(k, k+controlValue));
                                }else{
                                    cb.newlineShowText(prodotto[i][j].substring(k));
                                }
                            }
                        }
                    }else{
                        cb.moveText(misure[j], 0);
                        cb.newlineShowText(prodotto[i][j]);
                    }
                }else if(j==5||j==2){
                    cb.moveText(misure[j], 0);
                    Font sottolineato = new Font(FontFamily.HELVETICA, 12, Font.UNDERLINE);
                    //cb.setFontAndSize(sottolineato.getBaseFont(), (float) 10.5);
                    cb.newlineShowText(monetizza(Float.parseFloat(prodotto[i][j])));
                    cb.setFontAndSize(BaseFont.createFont(), (float) 10.5);
                }else{
                    cb.moveText(misure[j], 0);
                    cb.newlineShowText(prodotto[i][j]);
                }
            }
        }
        //Fine prodotti
        //fine tabella
        //Inizio totali
        if(Integer.parseInt(scontoTotale)!=0){
            cb.moveText(-90, -50);
            cb.newlineShowText("Sconto: "+scontoTotale+"%");
            cb.moveText(90, 0);
            cb.newlineShowText(monetizza((float) (Double.parseDouble(totale)/100*Integer.parseInt(scontoTotale))));
        }
        cb.moveText(-90, -20);
        cb.newlineShowText("Totale:");
        cb.moveText(90, 0);
        cb.newlineShowText(totale);
        //Fine totali
        //Inizio note
        if(!note.isEmpty()){
            cb.moveText(-490, -50);
            cb.newlineShowText("Note:");
            int controlValue = 100;
            if(note.length()>controlValue){
                for(int k=0;k<note.length();k+=controlValue){
                    if(k!=0){
                        cb.moveText(0, -20);
                        if(k+controlValue<=note.length()){                                    
                            cb.newlineShowText(note.substring(k, k+controlValue));
                        }else{
                            cb.newlineShowText(note.substring(k));
                        }
                    }else{
                        cb.moveText(0, -20);
                        if(k+controlValue<=note.length()){                                    
                            cb.newlineShowText(note.substring(k, k+controlValue));
                        }else{
                            cb.newlineShowText(note.substring(k));
                        }
                    }
                }
            }else{
                cb.moveText(0, -20);
                cb.newlineShowText(note);
            }
        }
        //Fine note
        //Inizio footer
        cb.setFontAndSize(BaseFont.createFont(), (float) 10.5);
        cb.setTextMatrix(document.right()/4+20, 30);
        cb.showText("Via Luserte 7 - 6572 Quartino +41 (0)76 616 67 73");
        //Fine footer
        cb.endText();
        
        // step 5
        document.close();
        Process p = Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+result+"");
        try {
            p.waitFor();
        } catch (InterruptedException ex) {
            Logger.getLogger(PdfFattura.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Done");
    }
}